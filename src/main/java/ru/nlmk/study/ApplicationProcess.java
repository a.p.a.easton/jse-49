package ru.nlmk.study;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.study.command.CommandInterface;
import ru.nlmk.study.sender.FormDataSender;
import ru.nlmk.study.visa.VisaApplicationForm;

import java.util.List;
import java.util.Map;

@Service
public class ApplicationProcess {

    private static final String EXIT = "exit";

    private static final String FORM = "form";

    private static final String RUSSIAVISA = "Russia";

    private static final String USAVISA = "USA";

    private final CommandInterface command;

    private final Map<String, VisaApplicationForm> visaMap;

    private final List<FormDataSender> sender;

    @Autowired
    public ApplicationProcess(CommandInterface command, Map<String, VisaApplicationForm> visaMap, List<FormDataSender> sender) {
        this.command = command;
        this.visaMap = visaMap;
        this.sender = sender;
    }

    public void process(){
        VisaApplicationForm visa;
        while (true){
            command.write("Type exit to exit the program");
            command.write("Type form to start filling the form");
            String enterCommand = command.writeAndRead("Enter command");
            switch (enterCommand){
                case (EXIT):
                    return;
                case (FORM):
                    String formType = command.writeAndRead("Which type of form do you want to fill, Russia or USA?");
                    if(formType.equals(USAVISA)){
                        visa = visaMap.get("USAVisaApplicationFormImpl");
                    }
                    else if (formType.equals(RUSSIAVISA)){
                        visa = visaMap.get("russiaVisaApplicationFormImpl");
                    }
                    else{
                        System.out.println("Entered type of form is not supported");
                        return;
                    }
                    visa.fillForm();
                    String formData = visa.getFormData();
                    for(FormDataSender s: sender){
                        s.send(formData);
                    }
                    break;
                default:
                    System.out.println("Entered command is not supported");
                    break;
            }
        }

    }
}
