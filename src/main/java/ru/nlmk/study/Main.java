package ru.nlmk.study;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppContext.class);
        ApplicationProcess applicationProcess = context.getBean(ApplicationProcess.class);
        applicationProcess.process();
    }
}
