package ru.nlmk.study.command;

public interface CommandInterface {
    /**
     * Отправить и получить данные из интерфейса.
     * @param text данные для отправки в интерфейс.
     * @return данные полученные от интерфейса.
     */
    String writeAndRead(String text);

    /**
     * Отправить данные в интерфейс.
     * @param text данные для отправки в интерфейс.
     */
    void write(String text);

}
