package ru.nlmk.study.command;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Primary
@Component
public class ConsoleCommandInterface implements CommandInterface{
    @Override
    public String writeAndRead(String text) {
        System.out.println(text);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    @Override
    public void write(String text) {
        System.out.println(text);
    }
}
