package ru.nlmk.study.command;

import org.springframework.stereotype.Component;

@Component
public class VoiceCommandInterface implements CommandInterface {
    @Override
    public String writeAndRead(String text) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void write(String text) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
}
