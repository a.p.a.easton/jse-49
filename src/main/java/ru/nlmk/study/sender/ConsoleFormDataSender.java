package ru.nlmk.study.sender;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
public class ConsoleFormDataSender implements FormDataSender {
    @Override
    public void send(String data) {
        System.out.println("ConsoleFormDataSender: " + data);
    }
}
