package ru.nlmk.study.sender;

import org.springframework.stereotype.Component;

@Component
public class FileFormDataSender implements FormDataSender {
    @Override
    public void send(String data) {
        System.out.println("FileFormDataSender: " + data);
    }
}
