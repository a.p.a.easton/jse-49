package ru.nlmk.study.visa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import ru.nlmk.study.command.CommandInterface;

@Primary
@Service
public class RussiaVisaApplicationFormImpl implements VisaApplicationForm {

    private CommandInterface command;
    private String formData;

    @Autowired
    public RussiaVisaApplicationFormImpl(CommandInterface command) {
        this.command = command;
    }

    @Override
    public void fillForm() {
        formData = command.writeAndRead("Please, enter the Russia Form");
    }

    @Override
    public String getFormData() {
        return formData;
    }
}
