package ru.nlmk.study.visa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.study.command.CommandInterface;

@Service
public class USAVisaApplicationFormImpl implements VisaApplicationForm {

    private CommandInterface command;

    private String formData;

    @Autowired
    public USAVisaApplicationFormImpl(CommandInterface command) {
        this.command = command;
    }

    @Override
    public void fillForm() {
        formData = command.writeAndRead("Please, enter the USA Form");
    }

    @Override
    public String getFormData() {
        return formData;
    }
}
